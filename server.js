let express = require('express');
let compression = require('compression');
let path = require('path');
let app = express();
let http = require('http');
let server = http.Server(app);
let cookieParser = require('cookie-parser');
let sendingActions = require('./server/sending-actions');
let systemActions = require('./server/system-actions');
let uploadActions = require('./server/upload-actions');
let log = require('./server/system/log')(module);
let production = process.env.NODE_ENV === 'production';
let listenServer = server;
let functions = require('./server/functions');
let httpPort = functions.getData().settings.port;
let busboy = require('connect-busboy');
let io = require('socket.io')(listenServer, {
  path: '/action2'
});
let constants = require('./server/constants');
process.env.UV_THREADPOOL_SIZE = 128;
global.io = io;

app.use(compression());
app.set('view cache', true);
app.use(express.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboy({
  highWaterMark: 2 * 1024 * 1024,
}));

app.get(constants.URL.DATA, systemActions.getData);

app.post(constants.URL.SAVE_DATA, systemActions.saveData);

app.post(constants.URL.STOP_SERVER, systemActions.stopServer);

app.post(constants.URL.START_SERVER, sendingActions.startServer);

app.post(constants.URL.UPLOAD_REQUEST, uploadActions.getRequestUploading);

app.post(constants.URL.UPLOAD, uploadActions.uploadFile);

app.post(constants.URL.EXTEND_UNLOCK, uploadActions.getRequestExtendUnlocking);

app.post(constants.URL.END_UPLOADING, uploadActions.getRequestEndUploading);

app.post(constants.URL.REVOKE, uploadActions.getRequestRevoke);

app.get('/snd/:pass', (req, res) => {
  if (req.params.pass === '422422') {
    res.cookie('GFefbFccxdRT', '1111111', {
      expires: new Date(Date.now() + 10e6)
    });
  }
  res.redirect('/');
});

app.get('/', (req, res) => {
  if (!req.cookies.GFefbFccxdRT)
    res.sendFile(path.join(__dirname, '/public/auth.html'));
  else
    res.sendFile(path.join(__dirname, '/public/index2.html'));
});

server.listen(process.env.PORT || httpPort, () => {
  log.info(`server running on port ${server.address().port}`);
});