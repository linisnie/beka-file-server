const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
require('@babel/polyfill');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// process.traceDeprecation = true;
module.exports = {
  mode: NODE_ENV,
  entry:{
    'js': [
      "@babel/polyfill", './frontend/libs/init.js', './frontend/app.js'
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  plugins:[
    // new ExtractTextPlugin({
    //   filename: NODE_ENV === 'development' ? "styles.css" : "styles."+Math.random()+".css"
    // }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: NODE_ENV === 'development' ? "styles.css" : "[hash].css",
      chunkFilename: NODE_ENV === 'development' ? "[id].css" : '[id].[hash].css'
    }),
    new HtmlWebpackPlugin({
      template: 'frontend/index.html',
      filename: __dirname + '/public/index2.html',
    }),
    // new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    rules: [{
      test: /\.(js|jsx)?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ["@babel/react", "@babel/env"],
          plugins: [
            '@babel/plugin-proposal-object-rest-spread',
            "@babel/plugin-proposal-class-properties",
            "@babel/plugin-syntax-dynamic-import"
          ]
        }
      }
    },
    {
      test: /\.(sa|sc|c)ss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            // you can specify a publicPath here
            // by default it use publicPath in webpackOptions.output
            publicPath: __dirname + '/public/',
            url: false,
            sourceMap: true,
            modules: true,
            importLoaders: true,
          }
        },
        {
          loader: 'css-loader',
          options: {
            // you can specify a publicPath here
            // by default it use publicPath in webpackOptions.output
            publicPath: __dirname + '/public/',
            url: false,
            sourceMap: true,
            modules: true,
            importLoaders: true,
          }
        },
        // 'css-loader',
        'sass-loader',
      ],
    },
    ]
  },
  watch: NODE_ENV === 'development',
  watchOptions: {
    aggregateTimeout: 100,
    poll: true,
    ignored: /node_modules/
  },
  devtool: NODE_ENV === 'development' ? 'cheap-inline-source-map' : '',
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/public',
    publicPath: '/',
    filename: NODE_ENV === 'development' ? 'app.js' : 'app.[chunkhash:5].js'
  },
  devServer: {
    contentBase: './public',
    hot: true
  }
};


