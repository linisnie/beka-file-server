import React, {Component} from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import Client from './app'
import CssBaseline from '@material-ui/core/CssBaseline';
import dayjs from 'dayjs';

// dayjs.locale('ru');


const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    suppressDeprecationWarnings: true
  },
  palette: {
    // primary: { main: '#006c2e' },
    // secondary: { main: '#D84315' },
    primary: { main: '#006064' },
    background: {default: '#d5d7db'}
  },
  overrides: {
    MuiButton: {
      root: {
        lineHeight: 'none',
        borderRadius: 4,
        height: 33,
        minHeight: 33,
        padding: '0 13px',
        // backgroundColor: '#2d2c2c',
        // color: 'white',
      },
      raised: {
        backgroundColor: '#2d2c2c',
        color: 'white',
        boxShadow: '0 1px 6px rgba(0,0,0,.12), 0 1px 4px rgba(0,0,0,.12)',
        '&:hover': {
          backgroundColor: '#1a1919',
          // Reset on mouse devices
          '@media (hover: none)': {
            backgroundColor: '#1a1919',
          },
        },
      },
      raisedPrimary: {
        backgroundColor: '#006064',
        color: 'white',
        '&:hover': {
          backgroundColor: 'rgb(0, 67, 70)',
          // Reset on mouse devices
          '@media (hover: none)': {
            backgroundColor: 'rgb(0, 67, 70)',
          },
        },
      },
      raisedSecondary: {
        backgroundColor: '#f50057',
        color: 'white',
        '&:hover': {
          backgroundColor: '#c51162',
          // Reset on mouse devices
          '@media (hover: none)': {
            backgroundColor: '#c51162',
          },
        },
      },
    },
    MuiTableCell: {
      body: {
        fontSize: 14,
      }
    }
  },
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
    action
      .post('common/error/log')
      .send({
        error: {
          target: 'all app',
          error: error.toString(),
          errorInfo: errorInfo
        }
      })
      .end();
  }


  render() {
    if (this.state.errorInfo) {
      return (
        <div>
          <center>
            <h2>Что-то пошло не так.</h2>
            {
              process.env.NODE_ENV !== 'production' &&
              <details style={{ whiteSpace: 'pre-wrap' }}>
                {this.state.error && this.state.error.toString()}
                <br />
                {this.state.errorInfo.componentStack}
              </details>
            }
          </center>
        </div>
      );
    }
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline/>
        <Client/>
      </MuiThemeProvider>
    )
  }
}
const mapActionsToProps =  {
};

const mapStateToProps = (state) => {
  return {
    point: state.getIn([ 'url', 'current', 'point']),
  }
};

export default connect(mapStateToProps, mapActionsToProps)(App);


