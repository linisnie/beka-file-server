import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import styles from './home-styles.js';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl/index';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import Input from '@material-ui/core/Input/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import Paper from '@material-ui/core/Paper/index';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar/index';
import Card from '@material-ui/core/Card/index';
import CardActions from '@material-ui/core/CardActions/index';
import CardContent from '@material-ui/core/CardContent/index';
import CardHeader from '@material-ui/core/CardHeader/index';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar/index';
import Typography from '@material-ui/core/Typography';
import {ComputerSettings, MainSettings} from "./Frames";
import Loading from "../../../components/loading";
import {addComputer, saveData, sendSignal} from "../../../actions/core-actions";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import StopIcon from '@material-ui/icons/Stop';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import CircularProgress from '@material-ui/core/CircularProgress';


let Home = props => {

  const {
    classes, loaded, data, addComputer, shouldSave, saveData, ioData,
    isServerActive, isServerSignalPrecessing, sendSignal
  } = props;
  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <div className={classes.heroContent}>
          <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
            Beka file server
          </Typography>
          <Typography variant="h6" align="center" color="textSecondary" component="p">
            Синхронизация add-файлов между разными компьютерами по http
          </Typography>
        </div>
        {
          !loaded && <Loading/>
        }
        {
          loaded &&
          <Grid container direction='column' justify="flex-start" className={classes.blocks} spacing={40}>
            <Grid item>
              <MainSettings data={data.get('settings')}/>
            </Grid>
            {
              data.get('computers').map((computer, key) =>
                <Grid key={key} item>
                  <ComputerSettings index={key}
                                    data={
                                      computer
                                        .merge(ioData.getIn(['computers',  computer.get('host')])
                                        )}/>
                </Grid>
                ).toIndexedSeq().toArray()
            }
          </Grid>
        }

      </main>
      <footer className={classNames(classes.footer, classes.layout)}>
        <Grid container spacing={32} justify="center">
          <Typography variant="h6" align="center" gutterBottom color="textSecondary">
            T.Makarov 2019
          </Typography>
        </Grid>
      </footer>
      <Fab onClick={() => isServerActive ? sendSignal('stop') : sendSignal('start')}
           color='primary'
           className={classNames(classes.fab, classes.stop)}
      >
        {
          [
            isServerActive ? <StopIcon/> : <PlayArrowIcon/>,
            <CircularProgress className={classes.progress} color="secondary" />,
          ][+isServerSignalPrecessing]
        }
      </Fab>
      <Fab onClick={saveData}
           disabled={!shouldSave}
           color='secondary'
           className={classNames(classes.fab, classes.save)}>
        <SaveIcon/>
      </Fab>
      <Fab onClick={addComputer} className={classes.fab} color='primary'>
        <AddIcon />
      </Fab>
    </React.Fragment>
  );

};

const mapStateToProps = state => ({
  data: state.getIn(['core', 'data']),
  ioData: state.getIn(['core', 'ioData']),
  shouldSave: state.getIn(['core', 'shouldSave']),
  loaded: state.getIn(['core', 'dataLoaded']),
  isServerSignalPrecessing: state.getIn(['core', 'serverStatusLoading']),
  isServerActive: state.getIn(['core', 'data', 'settings', 'active']),
});

const mapActionsToProps = {
  addComputer, saveData, sendSignal
};

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Home));