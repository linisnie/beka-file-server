import React, {Fragment, useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import styles from './frames-styles.js';
import CssBaseline from '@material-ui/core/CssBaseline/index';
import FormControl from '@material-ui/core/FormControl/index';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import Input from '@material-ui/core/Input/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import Paper from '@material-ui/core/Paper/index';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar/index';
import Card from '@material-ui/core/Card/index';
import CardActions from '@material-ui/core/CardActions/index';
import CardContent from '@material-ui/core/CardContent/index';
import CardHeader from '@material-ui/core/CardHeader/index';
import Grid from '@material-ui/core/Grid/index';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar/index';
import Typography from '@material-ui/core/Typography/index';
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import Collapse from '@material-ui/core/Collapse';
import {deleteComputer, setToData} from "../../../actions/core-actions";
import MenuItem from '@material-ui/core/MenuItem';
import dayjs from 'dayjs';
import Divider from '@material-ui/core/Divider';
import {IconButtonProgress} from "../../../components/progress-icon-button";
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

let CardElement = withStyles(styles)(props =>
  <Fragment>
    <Typography className={props.classes.title} color="textSecondary">
      {props.title}
    </Typography>
    <Typography variant="body1" component="h2" gutterBottom>
      {props.value}
    </Typography>
  </Fragment>
);

let Field = withStyles(styles)(props =>
  <TextField
    label={props.label}
    multiline={props.multiline}
    select={props.select}
    fullWidth
    className={props.classes.textField}
    onChange={props.onChange}
    value={props.value}
    margin="normal"
    variant="outlined"
    children={props.children}
  />
);

const mapStateToPropsMainSettings = state => ({
  uuid: state.getIn(['core', 'uuid'])
});

const mapActionsToPropsMainSettings = {
  setToData
};
export let MainSettings =
  connect(mapStateToPropsMainSettings, mapActionsToPropsMainSettings)(withStyles(styles)(props => {
  let [expanded, setExpanded] = useState(false);
  const {
    classes, data, setToData
  } = props;

  return (
    <Card className={classes.card}>
      <CardHeader
        title='Основные настройки'
        titleTypographyProps={{ align: 'left' }}
      />
      <CardContent>
        <CardElement title='Период обмена, сек' value={data.get('period')}/>
        <CardElement title='Уведомления в телеграм' value={data.get('telegramReceivers')}/>
        <CardElement title='Уведомлять после (мин)' value={data.get('notifyAfter')}/>
        <CardElement title='Имя этого сервера' value={data.get('name')}/>
      </CardContent>
      <CardActions>
        <Button color='primary' onClick={() => setExpanded(!expanded)} size="small">Настроить</Button>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Field
            label="Период обмена"
            onChange={e => setToData(['settings', 'period'], e.target.value)}
            value={data.get('period')}
          />
          <Field
            label="Уведомления в телеграм"
            onChange={e => setToData(['settings', 'telegramReceivers'], e.target.value)}
            value={data.get('telegramReceivers')}
          />
          <Field
            label="Уведомлять после (мин)"
            onChange={e => setToData(['settings', 'notifyAfter'], e.target.value)}
            value={data.get('notifyAfter')}
          />
          <Field
            label="Имя этого сервера"
            onChange={e => setToData(['settings', 'name'], e.target.value)}
            value={data.get('name')}
          />
        </CardContent>
      </Collapse>
    </Card>
  );
}));

const mapStateToPropsComputerSettings = state => ({
  uuid: state.getIn(['core', 'uuid'])
});

const mapActionsToPropsComputerSettings = {
  setToData, deleteComputer
};

let fields = [
  {title: 'Имя', id: 'name'},
  // {title: 'Последняя синхронизация', id: 'lastSync'},
  {title: 'Адрес', id: 'host'},
  {title: 'Порт', id: 'port'},
  {title: 'Куда загружать', id: 'inputDir'},
  {title: 'Откуда Отдавать', id: 'outputDir'},
];

let File = withStyles(styles)(props => {
  return (
    <div className={props.classes.file}>
      <IconButtonProgress size='small' loading progressProps={{value: props.progress, variant:'static'}}>
        <InsertDriveFileIcon fontSize='small'/>
      </IconButtonProgress>
      <Typography variant="caption" component="h2" gutterBottom>
        {props.title}
      </Typography>
    </div>

  )
});

export let ComputerSettings =
  connect(mapStateToPropsComputerSettings, mapActionsToPropsComputerSettings)(withStyles(styles)(props => {
    const {
      classes, setToData, data, index, deleteComputer
    } = props;
    let expanded = data.get('expanded');
    return (
      <Card className={classes.card}>
        <CardHeader
          title='Удаленный компьютер'
          titleTypographyProps={{ align: 'left' }}
        />
        <Grid container spacing={0}>
          <Grid item className={classes.gridItem} xs={6}>
            {
              fields.map((field, key) => <CardElement key={key} title={field.title} value={data.get(field.id)}/>)
            }
            <CardElement title='Файлы' value={data.get('syncFiles').join(', ')}/>
          </Grid>
          <Grid item className={classNames(classes.gridItem, classes.gridItemBorder)} xs={6}>
            <CardElement title='Синхронизация' value={data.get('enabled') ? 'Включена' : 'Выключена'}/>
            <CardElement title='Статус загрузки' value={data.get('uploadingStatus')}/>
            <CardElement title='Статус отправки' value={data.get('sendingStatus')}/>
            <CardElement title='Последняя загрузка'
                         value={data.get('lastUpload') !== 'None' ?
                           dayjs(data.get('lastUpload')).format('HH:mm:ss DD.MM.YY') : 'None'}
            />
            <CardElement title='Последняя отправка'
                         value={data.get('lastSent') !== 'None' ?
                           dayjs(data.get('lastSent')).format('HH:mm:ss DD.MM.YY') : 'None'}
            />
          </Grid>
          <Grid item className={classes.gridItem} xs={12}>
            <Divider/>
            <div className={classes.section}>
              <Typography className={props.classes.title} color="textSecondary">
                Отправка
              </Typography>
              {
                data.get('sending') &&
                data.get('sending')
                  .map((file, i) => <File key={i} progress={file.get('loaded')} title={file.get('name')}/>)
                  .toIndexedSeq().toArray()
              }
            </div>
            <Divider/>
            <div className={classes.section}>
              <Typography className={props.classes.title} color="textSecondary">
                Приём
              </Typography>
              {
                data.get('uploading') &&
                data.get('uploading')
                  .map((file, i) => <File key={i} progress={file.get('loaded')} title={file.get('name')}/>)
                  .toIndexedSeq().toArray()
              }
            </div>
          <Divider/>
          </Grid>
        </Grid>
        <CardContent>
        </CardContent>
        <CardActions>
          <Button color='primary'
                  onClick={() => setToData(['computers', +index, 'expanded'], !expanded, false)}
                  size="small"
          >Настроить</Button>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {
              fields.map((field, key) =>
                <Field
                  key={key}
                  label={field.title}
                  onChange={e => setToData(['computers', +index, field.id], e.target.value)}
                  value={data.get(field.id)}
                />
              )
            }
            <Field
              multiline
              label='Файлы'
              onChange={e => setToData(['computers', +index, 'syncFiles'], e.target.value.split(/,[\s\n]*/))}
              value={data.get('syncFiles').join(', ')}
            />
            <Field
              select
              label='Статус'
              onChange={e => setToData(['computers', +index, 'enabled'], e.target.value)}
              value={data.get('enabled')}
            >
              <MenuItem value={true}>
                Включен
              </MenuItem>
              <MenuItem value={false}>
                Выключен
              </MenuItem>
            </Field>
          </CardContent>
          <CardActions>
            <Button color='primary' onClick={() => deleteComputer(index)} size="small">Удалить</Button>
          </CardActions>
        </Collapse>
      </Card>
    );
  }));

