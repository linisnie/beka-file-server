const styles = theme => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  card: {
    // width: 300,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
  },
  file: {
    display: 'inline-flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: theme.spacing.unit * 2,
  },
  section: {
    margin: `${theme.spacing.unit}px 0`
  },
  gridItem: {
    padding: `${theme.spacing.unit * 2}px`
  },
  gridItemBorder: {
    borderLeft: `1px solid #d8d8d8`
  }
});

export default styles;