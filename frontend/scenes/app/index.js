import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles/index';
import { connect } from "react-redux";
import styles from './styles';
import Home from './components/home';
import Loading from "../../components/loading";


let Client = props => {

  const {
    classes, isMobile
  } = props;

  return (
    <div className={classes.root + ' ' + (isMobile ? classes.mobile : '')}>
      <Home/>
    </div>
  );
};

const mapStateToProps = state => ({
  isMobile: state.getIn(['core', 'settings', 'isMobile']),
});

const mapActionsToProps = {

};

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Client));