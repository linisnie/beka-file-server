const styles = theme => ({
 root: {
  width: 'auto',
  marginLeft: theme.spacing.unit * 3,
  marginRight: theme.spacing.unit * 3,
  [theme.breakpoints.up(1028 + theme.spacing.unit * 3 * 2)]: {
   width: 1028,
   marginLeft: 'auto',
   marginRight: 'auto',
  },
  position: 'relative',
  height: 'calc(100vh - 80px)',
  minHeight: '300px',
  marginBottom: '40px',
  marginTop: '40px',
 },
 mobile: {
  "height": "100%",
  "position": "absolute",
  "minHeight": "300px",
  width: '100%',
  marginBottom: 0,
  marginTop: 0,
  marginLeft: 0,
  marginRight: 0,
 },

});

export default styles;