import {LOAD_DATA, SET_TO_DATA, ADD_COMPUTER, DELETE_COMPUTER, SET_TO_CORE} from "../actions/core-actions";
import { Map, fromJS } from 'immutable';

const initialState = Map({});

export function coreReducer(state = initialState, {type, payload}) {
  switch (type) {
    case LOAD_DATA:
      return state.setIn(['data'], fromJS(payload.data)).setIn(['dataLoaded'], true);

    case SET_TO_DATA:
      return state.setIn(['data'].concat(payload.path), fromJS(payload.value))
        .setIn(['shouldSave'], payload.shouldSave);

    case SET_TO_CORE:
      return state.setIn(payload.path, fromJS(payload.value));

    case ADD_COMPUTER:
      return state.updateIn(['data', 'computers'], arr => arr.push(fromJS(payload.data)));

    case DELETE_COMPUTER:
      return state.updateIn(['data', 'computers'], arr => arr.delete(+payload.id));

    default:
      return state;
  }
}