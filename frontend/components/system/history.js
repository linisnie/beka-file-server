function createHistory() {
  let listener;
  let push = (path) => {
    if (!window.history.pushState) {
      location.href = path;
      return;
    }
    window.history.pushState(null, null, path);
    if (listener) listener();
  };

  let back = window.history.back;

  let replace = (path) => {
    if (!window.history.replaceState) {
      location.href = path;
      return;
    }
    window.history.replaceState(null, null, path);
    if (listener) listener();
  };

  let listen = (f) => {listener = f; window.onpopstate = f};

  let setIn = (parameter, value) => {
    let url = store.getState().getIn(['url', 'current']);
    let path = url.path.replace(/\?/g, '');
    let variables = path.match(/:([^/]+)/g).map(el => el.replace(':', ''));
    let parameters = {};
    for (let variable of variables) {
      parameters[variable] = decodeURIComponent(url.parameters[variable]);
    }
    if (typeof parameter === 'string')
      parameters[parameter] = value;
    else if (Array.isArray(parameter) && parameter.length === value.length)
      for (let i = 0; i < parameter.length; i++)
        parameters[parameter[i]] = value[i];
    else throw Error('history.setIn accept only string or array parameters');
    let toPath = pathToRegexp.compile(path)(parameters).replace('/undefined', '');
    history.push(toPath);
  };
  return {
    push,
    listen,
    setIn,
    replace,
    back
  }
}

export const history = createHistory({basename: '/'});