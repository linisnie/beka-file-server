import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import withStyles from '@material-ui/core/styles/withStyles';
import { makeStyles } from '@material-ui/styles';

let useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    display: 'inline-block',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: '-12px',
    marginLeft: '-12px',
  },
  fabProgress: {
    position: 'absolute',
    zZndex: '1',
    top: '0',
    left: '0',
  },
  small: {
    top: 6,
    left: 6,
  }
});

let ButtonProgress = props =>  {
  const classes = useStyles();
  return (
    <div style={props.wrapperStyle} className={classes.wrapper}>
      <Button
        {...props}
        disabled={props.loading}
      >
        {props.children}
      </Button>
      {props.loading && <CircularProgress size={24} className={classes.buttonProgress} />}
    </div>
  )
};

ButtonProgress.propTypes = {
  loading: PropTypes.bool,
  buttonProps: PropTypes.object,
  wrapperProps: PropTypes.object,
  wrapperStyle: PropTypes.object
};

ButtonProgress.defaultProps = {
  buttonProps: {},
  wrapperProps: {},
  wrapperStyle: {}
};

let IconButtonProgress = props =>  {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <IconButton
        disabled={props.loading}
        {...props.iconButtonProps}
      >
        {props.children}
      </IconButton>
      {props.loading && <CircularProgress size={props.circularSize[props.size]}
                                          {...props.progressProps}
                                          className={classes.fabProgress + ' ' + classes[props.size]} />}
    </div>
  )
};

IconButtonProgress.propTypes = {
  iconButtonProps: PropTypes.object,
  progressProps: PropTypes.object,
  size: PropTypes.oneOf(['small', 'large', 'medium'])
};

IconButtonProgress.defaultProps = {
  size: 'large',
  circularSize: {
    large: 48,
    medium: 37,
    small: 33
  }
};

// IconButtonProgress = withStyles(styles)(IconButtonProgress);

export {IconButtonProgress};

export default ButtonProgress;