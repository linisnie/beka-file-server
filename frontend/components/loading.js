import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import React from "react";
import PropTypes from 'prop-types';

let Loading = props => (
  <div style={{
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: props.position,
    background: "#ffffffa3",
    zIndex: '1',
    padding: 24,
    boxSizing: 'border-box'
  }}>
    <CircularProgress size={props.size} />
  </div>
);

Loading.propTypes = {
  size: PropTypes.number,
  position: PropTypes.oneOf(['start', 'center'])
};

Loading.defaultProps = {
  size: 50,
  position: 'center'
};


export default Loading;