export const LOAD_DATA = 'core:LOAD_DATA';
export const SET_TO_DATA = 'core:SET_TO_DATA';
export const SET_TO_CORE = 'core:SET_TO_CORE';
export const ADD_COMPUTER = 'core:ADD_COMPUTER';
export const DELETE_COMPUTER = 'core:DELETE_COMPUTER';
export const SERVER_SINGAL_PROCESSING = 'core:SERVER_SINGAL_PROCESSING';
import {store} from "../reducers/reducer";
import dayjs from 'dayjs';
import {history} from "../components/system/history";
import request from 'superagent';
import constants from '../../server/constants';

/**
 * Subscribe actions to socket.io
 * @param socket {object} - socket.io socket
 * @param store {object} - redux store
 */
export function subscribeCoreToIO(socket, store) {
  socket.on('message', msg => setToCore(msg.path, msg.value)(store.dispatch));
}

request
  .get('/data')
  .end((err, res) => {
    store.dispatch(dispatchSetData(res.body))
  });

let dispatchSetData = (data) => ({type: LOAD_DATA, payload: {data}});

/**
 * Set some value to ['core', 'data'].concat(path)
 * @param path {array}
 * @param value {*}
 * @param [shouldSave=true] {boolean} - should activate save button
 * after setting value?
 * @returns {Function}
 */
export function setToData(path, value, shouldSave = true) {
  return dispatch => {
    dispatch(dispatchSetToData(path, value, shouldSave));
  }
}

let dispatchSetToData = (path, value, shouldSave) => ({type: SET_TO_DATA, payload: {path, value, shouldSave}});

/**
 * Set some value to redux by path
 * @param path {array}
 * @param value {*}
 * @returns {Function}
 */
export function setToCore(path, value) {
  return dispatch => {
    dispatch(dispatchSetToCore(path, value));
  }
}

let dispatchSetToCore = (path, value) => ({type: SET_TO_CORE, payload: {path, value}});

/**
 * Add new computer to data.json
 * @returns {Function}
 */
export function addComputer() {
  return dispatch => {
    dispatch(dispatchAddComputer({
      "port": "8080",
      "host": "111.111.111.111",
      "name": "name",
      "syncFiles": ["addtovar.dbf", 'addtotyp.dbf', 'addcodes.dbf', 'adddlist.dbf', 'adddocum.dbf'],
      "enabled": true,
      "lastSync": "None",
      "inputDir": ".../In",
      "outputDir": ".../Out"
    }));
  }
}

let dispatchAddComputer = data => ({type: ADD_COMPUTER, payload: { data }});

/**
 * Delete computer from data.json by its array index
 * @param id {number} - array index
 * @returns {Function}
 */
export function deleteComputer(id) {
  return dispatch => {
    dispatch(dispatchDeleteComputer(id));
  }
}

let dispatchDeleteComputer = id => ({type: DELETE_COMPUTER, payload: {id}});

/**
 * Save data from redux to data.json
 * @returns {Function}
 */
export function saveData() {
  return dispatch => {
    let data = store.getState().getIn(['core', 'data'])
      .updateIn(['computers'], arr => arr.map(el => el.delete('expanded'))).toJS();
    request
      .post('/save-data')
      .send({data})
      .end(() => {
        dispatch(dispatchSetToCore(['shouldSave'], false));
      });
  }
}

/**
 * Send signal to start/stop server
 * @param signal {'start'|'stop'}
 * @returns {Function}
 */
export function sendSignal(signal) {
  return dispatch => {
    let url = {
      'stop': constants.URL.STOP_SERVER,
      'start': constants.URL.START_SERVER,
    }[signal];
    dispatch(dispatchSetToCore(['serverStatusLoading'], true));
    request
      .post(url)
      .end(() => {});
  }
}