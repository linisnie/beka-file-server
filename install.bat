call npm i pm2 -g
call npm i webpack -g
call npm i webpack-cli -g
call copy data.template.json data.json
call npm install pm2-windows-startup -g
call npm install
call pm2 install pm2-logrotate
call pm2 start ecosystem.config.js
call pm2 set pm2-logrotate:max_size 1M
call pm2 set pm2-logrotate:retain 1000
call pm2-startup install
call pm2 save
call webpack -p
pause