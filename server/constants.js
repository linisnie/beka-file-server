let path = require('path');
module.exports = {
  DIR_NOT_EMPTY: 'директория не пустая',
  DIR_IN_NOT_EMPTY: 'входящая директория не пустая',
  DIR_OUT_NOT_EMPTY: 'исходящая директория не пустая',
  DIR_LOCKED: 'директория заблокирована',
  DIR_IN_LOCKED: 'входящая директория заблокирована',
  SIGNAL_DOES_NOT_EXISTS: 'Отсутствует файл signal.dbf',
  DIR_OUT_LOCKED: 'исходящая директория заблокирована',
  DIR_NOT_EXIST: 'директория не существует',
  LOCK_FILE: 'signal.dbf',
  LOCKED_BY_BEKA_DBF: 'lockedbybeka.dbf',
  LOCKED_BY_1C: 'lockedby1c.dbf',
  DATA_PATH: path.join(__dirname, '..', 'data.json'),
  BACKUP_DIR_NAME: 'backup',
  BACKUP_DATA_PATH: path.join(__dirname, '..', 'data.backup.json'),
  UNLOCK_TIMER_PERIOD: 3*1000*60, /*3 min*/
  URL: {
    DATA: '/data',
    UPLOAD: '/upload',
    SAVE_DATA: '/save-data',
    UPLOAD_REQUEST: '/upload-request',
    EXTEND_UNLOCK: '/extend-unlock',
    END_UPLOADING: '/end-uploading',
    REVOKE: '/revoke',
    STOP_SERVER: '/stop-server',
    START_SERVER: '/start-server',
  },
  IO_ACTIONS: {
    LOG: 'log',
  },
  status: {
    SENDING: 'отправляем файлы',
    SENT: 'файлы отправлены',
    UPLOADING: 'загружаем файлы',
    UPLOADED: 'файлы загружены',
    STOPPED: 'сервер остановлен',
    SYNC_DISABLED: 'синхронизация выключена'
  },
  process: {
    isRunning: false,
    shutDownSignalReceived: false,
    sendingTo: [],
    receivingFrom: [],
  },
};