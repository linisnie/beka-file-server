let path = require('path');
let fs = require('fs-extra');
let constants = require('./constants');
let log = require('./system/log')(module);
let functions = require('./functions');
let dayjs = require('dayjs');
const { DIR_OUT_LOCKED } = require('./constants');

/**
 * Sends file by http
 * @param filePath {string} local - file path
 * @param host {string} - remote ip address
 * @param port {string} - remote port
 * @returns {Promise<object>} - remote server response
 */
async function sendFile(filePath, host, port) {
  let fileName = filePath.match(/[^\\/]+$/)[0];
  let readStream = fs.createReadStream(path.join(filePath));
  let stats = fs.statSync(path.join(filePath));
  log.info(`Sending ${fileName} (${stats.size} b) to ${host}`);
  let bytes = 0;
  let size = fs.lstatSync(path.join(filePath)).size;
  let percent = 0, lastPercent = 0;
  readStream.on('data', (chunk) => {
    let sent = bytes += chunk.length;
    percent = Math.round(sent/size*100);
    if (percent > lastPercent + 5 || (percent === 100 && lastPercent !== 100)) {
      lastPercent = percent;
      functions.ioMessage(
        {host, path: ['ioData', 'computers', host, 'sending', fileName],
        value: {name: fileName, loaded: percent}}
      );
    }
  });
  let formData = {
    file: readStream,
  };
  return await functions.post(`http://${host}:${port}${constants.URL.UPLOAD}`, {formData});
}

function sendRequestUploading(host, port) {
  return functions.post(`http://${host}:${port}${constants.URL.UPLOAD_REQUEST}`, {});
}

function sendRequestExtendUnlocking(host, port) {
  return functions.post(`http://${host}:${port}${constants.URL.EXTEND_UNLOCK}`, {});
}

function sendRequestEndUploading(host, port) {
  return functions.post(`http://${host}:${port}${constants.URL.END_UPLOADING}`, {});
}

function sendRequestRevoke(host, port) {
  return functions.post(`http://${host}:${port}${constants.URL.REVOKE}`, {});
}

let timers = {};

/**
 * Main function to handle computer
 * @param host {string} - remote ip address
 * @returns {Promise<void>}
 */
async function worker(host) {
  if (!constants.process.isRunning) return;
  let {outputDir, port, syncFiles, lastSent, name, enabled} = functions.getSettingsByIp(host);
  if (!enabled) {keep(host); return;}
  log.info(`****************************************************************`);
  log.info(`starting worker for ${host}`);
  let isDirEmpty;
  try {
    isDirEmpty = functions.isDirEmpty(outputDir, syncFiles);
  } catch(e) {
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'sendingStatus'], value: `${constants.DIR_NOT_EXIST}`});
    keep(host);
    return;
  }

  if (isDirEmpty) {
    log.info(`nothing to send from ${outputDir}`);
    keep(host);
    return;
  }
  let fd = functions.lockSync(outputDir);
  if (fd === false) {
    log.info(`${outputDir} locked`);
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'sendingStatus'], value: constants.DIR_OUT_LOCKED});
    functions.updateSettingsByIp(host, {sendingStatus: constants.DIR_OUT_LOCKED});
    keep(host);
    return;
  }
  let sendingAllowed = await sendRequestUploading(host, port);
  if (sendingAllowed.status !== 'ok') {
    log.info(`uploading to ${host} not allowed, ${sendingAllowed.message}`);
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'sendingStatus'], value: `${sendingAllowed.message}`});
    functions.unlockSync(fd, outputDir);
    functions.notifyError('send', lastSent, host, name, sendingAllowed.message);
    keep(host);
    return;
  }
  let uploaded;
  functions.updateSettingsByIp(host, {sendingStatus: constants.status.SENDING});
  functions.ioMessage({host, path: ['ioData', 'computers', host, 'sendingStatus'], value: constants.status.SENDING});
  functions.addToSendingList(host);
  clearTimeout(timers[host]);
  addTimeout(host, port);
  let dirSyncFiles = functions.getSyncFiles(outputDir, syncFiles);
  functions.backUpFiles(outputDir, dirSyncFiles);
  uploaded = await Promise.all(dirSyncFiles.map(file => {
    let filePath = path.join(outputDir, file);
    if (fs.existsSync(filePath))
      return sendFile(filePath, host, port);
    else
      return { status: 'ok', message: 'file not exist', filename: file};
  }));
  let allUploaded = uploaded.every(el => el && el.status === 'ok');
  clearTimeout(timers[host]);
  if (allUploaded) {
    await sendRequestEndUploading(host, port);
    functions.updateSettingsByIp(host, {lastSent: dayjs().format(), sendingStatus: constants.status.SENT});
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'sendingStatus'], value: constants.status.SENT});
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'lastSent'], value: dayjs().format()});
    functions.ioMessage({host, path: ['ioData', 'computers', host, 'sending'], value: {}});
    functions.deleteFromSending(host);
    if (functions.removeFromErrors('send', host))
      functions.notifySuccess(lastSent, host, name);

    functions.unlockSync(fd, outputDir);
    let data = functions.getData();
    if (!data.settings.debugMode)
      functions.clearDirSync(outputDir);
    keep(host);
  }
  else {
    await sendRequestRevoke(host, port);
    functions.unlockSync(fd, outputDir);
    functions.deleteFromSending(host);
    keep(host);
  }
  log.info(`****************************************************************`);
}

function addTimeout(host, port) {
  timers[host] = setTimeout(() => {
    sendRequestExtendUnlocking(host, port);
    addTimeout(host, port);
  }, constants.UNLOCK_TIMER_PERIOD - 500);
}

/**
 * Restarts worker for computer after period from settings
 * @param host {string} - ip address
 */
function keep(host) {
  if (timers[host])
    clearTimeout(timers[host]);
  if (constants.process.shutDownSignalReceived) return;
  let period = functions.getSettings().period * 1000;
  timers[host] = setTimeout(() => worker(host), period);
}

/**
 * Init workers for all computers with settings.enabled = true
 */
function startServer(req, res) {
  if (constants.process.isRunning === true) return;
  constants.process.isRunning = true;
  constants.process.shutDownSignalReceived = false;
  let data = functions.getData();
  data.settings.active = true;
  functions.saveData(data);
  let computers = data.computers;
  computers.map(el => worker(el.host));
  if (res) {
    functions.ioMessage({path: ['data', 'settings', 'active'], value: true});
    functions.ioMessage({path: ['serverStatusLoading'], value: false});
    res.end(JSON.stringify({status: 'ok'}));
  }
}

setTimeout(() => {
  let data = functions.getData();
  if (data.settings.active) {
    startServer();
    constants.process.isRunning = true;
  }
},400);

module.exports = {
  sendRequestRevoke,
  startServer,
  worker
};