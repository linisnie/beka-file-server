let functions = require('./functions');
let constants = require('./constants');
let sendingActions = require('./sending-actions');
let log = require('./system/log')(module);

/**
 * Get data from data.json
 * @param req
 * @param res
 */
function getData(req, res) {
  res.sendFile(constants.DATA_PATH);
}

/**
 * Save data to data.json
 * @param req
 * @param res
 */
function saveData(req, res) {
  try {
    functions.saveData(req.body.data);
    functions.backUpData(req.body.data);
    res.send({status:'ok'})
  }
  catch (e) {
    log.error(e);
    res.set(500).end();
  }
}

/**
 * Stops all workers
 * @param req
 * @param res
 */
let timer;

/**
 * Stop all workers
 * @param req
 * @param res
 * @param cb {function} - callback
 * @param shouldSave {boolean} - save "stop" sate to data.json
 */
function stopServer(req, res, cb, shouldSave = true) {
  let {sendingTo, receivingFrom} = constants.process;
  if (constants.process.shutDownSignalReceived) {
    if (receivingFrom.length !== 0 || sendingTo.length !== 0) {
      timer = setTimeout(() => stopServer(void 0, void 0, cb, shouldSave), 1000);
      log.error(`can't stop server, ${receivingFrom}, ${sendingTo}`);
      return;
    }
    else {
      clearTimeout(timer);
      if (shouldSave) {
        let data = functions.getData();
        data.settings.active = false;
        functions.saveData(data);
      }
      log.info('server stopped');
      functions.ioMessage({path: ['data', 'settings', 'active'], value: false});
      functions.ioMessage({path: ['serverStatusLoading'], value: false});
      constants.process.isRunning = false;
      constants.process.shutDownSignalReceived = false;
      if (cb) cb();
    }
  }
  if (constants.process.isRunning) {
    timer = setTimeout(() => stopServer(void 0, void 0, cb, shouldSave), 1000);
    log.info('Stop server signal received');
    constants.process.shutDownSignalReceived = true;
    if (res)
      res.send({status:'ok'});
  }
}

process.on('message', (msg) => {
  if (msg === 'shutdown')
    process.emit("SIGINT");
});

if (process.platform === "win32") {
  let rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.on("SIGINT", function () {
    process.emit("SIGINT");
  });
}

process.on('SIGINT', () => {
  log.info('SIGTERM signal received. Please wait until all jobs finished');
  stopServer(void 0, void 0, () => process.exit(0), false);
});

/**
 * Fix errors on server startup.
 * Clears all directories with no ended synchronisation
 */
function checkErrors() {
  let data = functions.getData();
  let withReceivingErrors = data.computers.filter(computer =>
    computer.enabled && (
      computer.uploadingStatus === constants.status.UPLOADING
    )
  );
  for (let computer of withReceivingErrors)
    try {
      functions.clearDirSync(computer.inputDir);
    }
    catch(e){}

  let withSendingErrors = data.computers.filter(computer =>
    computer.enabled && (
      computer.sendingStatus === constants.status.SENDING
    )
  );
  for (let computer of withSendingErrors)
    try {
      sendingActions.sendRequestRevoke(computer.host, computer.port);
    }
    catch(e){}
}
checkErrors();

module.exports = {
  saveData,
  getData,
  stopServer
};