let dayjs = require('dayjs');

function getLogger(module) {
  if (!module) console.error(`Module not defined in logger`);
  let pathModule = module.filename.split(/[\\/]/).slice(-2).join('/');
  return {
    log: str => console.log(`${dayjs().format('HH:mm:ss DD.MM.YY')} - log: [${pathModule}]`, str),
    info: str => console.info(`${dayjs().format('HH:mm:ss DD.MM.YY')} [${pathModule}]:`, str),
    error: str => {
      console.error(`${dayjs().format('HH:mm:ss DD.MM.YY')} - error: [${pathModule}]`, str);
    },
  }
}

module.exports = getLogger;