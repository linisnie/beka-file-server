let shttps = require('socks5-https-client');
const isReachable = require('is-reachable');
let fs = require('fs');

let testChatId = '24884548';
const GEMINI_ORDERS_TOKEN = '460594223:AAEtTLVM4s0r0y_hHT_DpQhZBDY98r__onY';
const GEMINI_ERRORS_TOKEN = '727759853:AAGvJRlSHqUg3WFX3eu7Fujjv63ZzVZn15k';
const GEMINI_ERRORS_MAKAROV_ID = '24884548';


async function sendTelegramMessage(
  message, chatId = testChatId, token = GEMINI_ORDERS_TOKEN, parseMode = '&parse_mode=Markdown'
) {
  let availableProxy = JSON.parse(fs.readFileSync(__dirname + '/proxy-servers.json', 'utf8'));
  let server = await getWorkingProxy(availableProxy);
  if (!server) return;
  let url = "/bot" + token + "/sendMessage?chat_id=" + chatId;
  url = url + "&disable_web_page_preview=True" + parseMode + "&text=" + encodeURIComponent(message);
  shttps.get({
    hostname: 'api.telegram.org',
    path: url,
    ...server
  }, function(res) {
    res.setEncoding('utf8');
    res.on('readable', function() {
      // console.log(res.read()); // Log response to console.
    });
  });
  return void 0;
}

async function getWorkingProxy(proxyArray) {
  for (let row of proxyArray)
    if (await isReachable(`${row.socksHost}:${row.socksPort}`))
      return row;
  return false;
}
module.exports.sendTelegramMessage = sendTelegramMessage;
module.exports.GEMINI_ERRORS_TOKEN = GEMINI_ERRORS_TOKEN;
module.exports.GEMINI_ERRORS_MAKAROV_ID = GEMINI_ERRORS_MAKAROV_ID;