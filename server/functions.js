let path = require('path');
let fs = require('fs-extra');
let log = require('./system/log')(module);
let request = require('request');
let constants = require('./constants');
let telegram = require('./system/telegram');
let dayjs = require('dayjs');

/**
 * Get data from data.json
 * @returns {json|boolean}
 */
function getData() {
  let data;
  try {
    data = JSON.parse(fs.readFileSync(constants.DATA_PATH));
  }
  catch (e) {
    log.error(e);
  }
  if (!data)
    try {
      data = JSON.parse(fs.readFileSync(constants.BACKUP_DATA_PATH));
      fs.unlinkSync(constants.DATA_PATH);
      saveData(data);
    } catch(e) {
      log.error(e);
      return false;
    }
  return data;
}

/**
 * Save data to data.json
 * @param data {object}
 */
function saveData(data) {
  fs.writeFileSync(constants.DATA_PATH, JSON.stringify(data, null, 2));
}

/**
 * Save data to data.backup.json
 * @param data {object}
 */
function backUpData(data) {
  fs.writeFileSync(constants.BACKUP_DATA_PATH, JSON.stringify(data, null, 2));
}

/**
 * Backup files to backup directory before sending
 * @param dirPath {string}
 * @param syncFiles {Array<string>}
 */
function backUpFiles(dirPath, syncFiles) {
  const backupRootDir = path.join(dirPath, constants.BACKUP_DIR_NAME);
  if (!fs.existsSync(backupRootDir))
    fs.mkdirSync(backupRootDir);
  let dir = fs.readdirSync(backupRootDir);
  dir = dir.sort((a,b) => a > b ? -1 : 0);
  dir = dir.filter(el => {
    const date = el.substring(0,10).replace(/_/g, '-');
    return dayjs().diff(dayjs(date), 'day') > 5;
  })
  const timestamp = dayjs().format('YYYY_MM_DD--HH_mm_ss');
  const backupDir = path.join(backupRootDir, timestamp);
  fs.mkdirSync(backupDir);
  let files = fs.readdirSync(dirPath);
  files = files.filter(file => syncFiles.includes(file));
  files.map(file => {
    const filePath = path.join(dirPath, file);
    const backupFilePath = path.join(backupDir, file);
    try {
      fs.copyFileSync(filePath, backupFilePath);
      try {
        fs.chmodSync(backupFilePath, 0o777);
      } catch (e) {
        log.error(`can't chmod file`, e);
      }

    }
    catch (e) {
      log.error(e);
    }
  });
  dir.map(el => fs.remove(path.join(backupRootDir, el)));
}

/**
 * Lock directory by opening signal.dbf
 * @param dirPath {string} - directory path
 * @returns {boolean|number} - file descriptor
 */
function lockBySignalDBF(dirPath) {
  let fd;
  try {
    fd = fs.openSync(path.join(dirPath, 'signal.dbf'), 'rs+');
  }
  catch (err) {
    if (err && err.code === 'EBUSY'){
      return false;
    }
    log.error(err);
    return false;
  }
  return fd
}

/**
 * Lock directory by creating lockedByBeka.dbf
 * @param dirPath {string} - directory path
 * @returns {boolean}
 */
function lockByLockedByBekaDBF(dirPath) {
  fs.writeFileSync(path.join(dirPath, constants.LOCKED_BY_BEKA_DBF), ' ');
  if (fs.existsSync(path.join(dirPath, constants.LOCKED_BY_1C))) {
    log.info(`dir already locked: ${dirPath}`);
    fs.unlinkSync(path.join(dirPath, constants.LOCKED_BY_BEKA_DBF));
    return false
  }
  return true;
}

/**
 * Lock directory
 * @param dirPath {string} - directory path
 * @returns {boolean|number} - file descriptor
 */
function lockSync(dirPath){
  log.info(`locking local dir: ${dirPath}`);
  const settings = getSettings();
  if (settings.mode === '1C')
    return lockByLockedByBekaDBF(dirPath);

  return lockBySignalDBF(dirPath);
}

/**
 * Unlock directory by closing signal.dbf
 * @param fd {number} - file descriptor
 * @param dirPath {string} - path to file
 * @returns {boolean}
 */
function unlockSync(fd, dirPath) {
  if (arguments.length < 2) throw Error('to few arguments');
  const settings = getSettings();
  if (typeof fd === "boolean" && settings.mode === 'beka') return false;
  log.info(`unlocking local dir: ${dirPath}`);
  try {
    if (settings.mode === '1C') {
      fs.unlinkSync(path.join(dirPath, constants.LOCKED_BY_BEKA_DBF));
      return true;
    } else
    fs.closeSync(fd);
    return true;
  }
  catch (err) {
    log.error(err);
    return false;
  }
}

/**
 * Clears directory except signal.dbf
 * @param dirPath
 * @returns {boolean}
 */
function clearDirSync(dirPath) {
  log.info(`clearing local dir: ${dirPath}`);
  let files = fs.readdirSync(dirPath).filter(file => {
    if (file.toLowerCase() === constants.LOCK_FILE) return false;
    if (file === constants.LOCKED_BY_BEKA_DBF) return false;
    if (!file.match(/\./)) return false;
    return true;
  });
  files.map(file => {
    try {
      fs.unlinkSync(path.join(dirPath, file));
      log.info(`${dirPath}${file} deleted`);
    }
    catch (e) {
      log.error(e);
    }
  });
  return true;
}

/**
 * Get settings for remote computer be its ip address
 * @param ip {string} - ip address
 * @returns {boolean|json}
 */
function getSettingsByIp(ip) {
  let data;
  try {
    data = getData();
  }
  catch (e) {
    return false;
  }
  return (data.computers.filter(computer => computer.host === ip) || [])[0];
}

/**
 * Get main settings from data.json
 * @returns {boolean|json}
 */
function getSettings() {
  let data;
  try {
    data = getData();
  }
  catch (e) {
    return false;
  }
  return data.settings;
}

/**
 * Update computer settings by its ip, and save to data.json
 * @param ip {string} - ip address
 * @param settings {object} - new settings
 * @returns {boolean}
 */
function updateSettingsByIp(ip, settings) {
  let newSettings = getData();
  let index = newSettings.computers.findIndex(e => e.host === ip);
  newSettings.computers[index] = {...newSettings.computers[index], ...settings};
  fs.writeFileSync(constants.DATA_PATH, JSON.stringify(newSettings, null, 2));
  return true;
}

/**
 * Extract remote ip address from request headers
 * @param req
 * @returns {string} - ip address
 */
function getIp(req) {
  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  return ip.replace(/[^0-9.]/g, '');
}

/**
 * Send post request to remote computer
 * @param url {string} - http://host:port/...
 * @param {object} [options] - request options
 * @param {object} [options.body] - request body
 * @param {object} [options.formData] - attached files
 * @returns {Promise<object>} - remote computer response
 */
function post(url, {body, formData}) {
  return new Promise((resolve => {
    request
      .post({url, formData, body}, (err, res, body) => {
        if (err) {
          if (err.message === 'ETIMEDOUT')
            resolve({status: 'error', message: 'Сервер недоступен'});
          else {
            resolve({status: 'error', message: err.message});
            log.error(err)
          }
        }
        else {
          let res;
          try {
            res = JSON.parse(body);
            resolve(res);
          }
          catch (e) {
            resolve({status: 'error', message: e.message});
            log.error(e);
          }
        }
      });
    setTimeout(() => {
      resolve({status: 'error', message: 'Запрос висел 20 минут и был завершён'});
    }, 1000 * 60 * 20)
  }))
}

/**
 * Emmit message by socket.io to frontend
 * @param {object} options - request options
 * @param {string} [options.host] - remote computer ip
 * @param {string|object} options.value - message to fronted
 * @param {array} options.path - path for redux (immutable.js)
 * @param {string} [options.action] - attached files
 */
function ioMessage ({host, value, path, action}) {
  io.emit('message', {host, value, path, action});
}

/**
 * Check if directory has any file from sync files list
 * @param dirPath {string} - directory path
 * @param syncFiles {array} - list of sync files
 * @returns {boolean}
 */
function isDirEmpty(dirPath, syncFiles) {
  let dir = fs.readdirSync(dirPath).map(el => el.toLowerCase());
  syncFiles = syncFiles.map(el => el.toLowerCase());
  return !syncFiles.some(el => dir.includes(el));
}

/**
 * Get filenames from directory
 * @param dirPath {string} - directory path
 * @param syncFiles {array} - list of sync files
 * @returns {array}
 */
function getSyncFiles(dirPath, syncFiles) {
  let dir = fs.readdirSync(dirPath);
  return syncFiles
    .map(syncFile => dir.find(dirFile => dirFile.toLowerCase() === syncFile.toLowerCase()) || syncFile)
}

/**
 * Deletes computer from list of active receiving processes
 * @param host {string} - ip address
 */
function deleteFromReceiving(host) {
  constants.process.receivingFrom = constants.process.receivingFrom.filter(el => el !== host);
}

/**
 * Adds computer to list of active receiving processes
 * @param host {string} - ip address
 */
function addToReceivingList(host) {
  constants.process.receivingFrom.push(host);
}

/**
 * Deletes computer from list of active sending processes
 * @param host {string} - ip address
 */
function deleteFromSending(host) {
  constants.process.sendingTo = constants.process.sendingTo.filter(el => el !== host);
}

/**
 * Adds computer to list of active sending processes
 * @param host {string} - ip address
 */
function addToSendingList(host) {
  constants.process.sendingTo.push(host);
}

/**
 * Send notification to telegram bot
 * @param message {string}
 * @returns {Promise<*>}
 */
async function sendTelegramNotification(message) {
  let data = getData();
  let receivers = (data.settings || {}).telegramReceivers;
  if (!receivers) return;
  receivers = receivers.split`,`.map(el => el.replace(/[^0-9]/g, ''));
  receivers.map(async el => {
      try {
        await telegram.sendTelegramMessage(message, el, telegram.GEMINI_ERRORS_TOKEN, '')
      } catch (e) {
        log.error(`can't send tg message`)
        log.error(e);
      }
    }
  );
  return void 0;
}

/**
 * Difference between two dates in minutes
 * @param date1 {string}
 * @param date2 {object|string}
 * @returns {number} - minutes
 */
function dateDiff(date1, date2 = dayjs()) {
  return date2.diff(dayjs(date1), 'minute');
}

let sendingErrors = {};
let uploadingErrors = {};

/**
 * Notify
 * @param type {'send','upload'}
 * @param lastSync {string}
 * @param host {string} - receiver server ip
 * @param name {string} - receiver server name
 * @param message {string}
 */
function notifyError(type, lastSync, host, name, message = '') {
  let data = getData();
  let minutes = (data.settings || {}).notifyAfter || 300;
  let thisServerName = data.settings.name;
  if (type === 'send' && dateDiff(lastSync) > minutes && !sendingErrors[host]) {
    sendTelegramNotification([
      'Ошибка отправки',
      'на сервере: ' + thisServerName,
      'отправка на сервер: ' + host,
      'имя: ' + name,
      'последняя синхронизация: ' + dayjs(lastSync).format('HH:mm:ss DD.MM.YYYY'),
      message
    ].join('\n'));
    sendingErrors[host] = true;
  }
  if (type === 'upload' && dateDiff(lastSync) > minutes && !uploadingErrors[host]) {
    sendTelegramNotification([
      'Ошибка загрузки',
      'На сервере: ' + thisServerName,
      'Загрузка с сервера: ' + host,
      'Имя: ' + name,
      'Последняя синхронизация: ' + dayjs(lastSync).format('HH:mm:ss DD.MM.YYYY'),
      message
    ].join('\n'));
    uploadingErrors[host] = true;
  }
}

/**
 * Notify success
 * @param lastSync {string}
 * @param host {string} - receiver server ip
 * @param name {string} - receiver server name
 */
function notifySuccess(lastSync, host, name) {
  let data = getData();
  let thisServerName = data.settings.name;

  sendTelegramNotification([
    'Файлы успешно отправлены',
    'С сервера: ' + thisServerName,
    'На сервер: ' + host + ' , ' + name,
    'Последняя синхронизация: ' + dayjs(lastSync).format('HH:mm:ss DD.MM.YYYY'),
  ].join('\n'));

}

/**
 * Remove host from errors list
 * @param type {'send','upload'}
 * @param host {string}
 * @returns {boolean}
 */
function removeFromErrors(type, host) {
  if (type === 'send' && sendingErrors[host]) {
    sendingErrors[host] = void 0;
    return true;
  }
  if (type === 'upload' && uploadingErrors[host]) {
    uploadingErrors[host] = void 0;
    return true;
  }
  return false;
}

module.exports = {
  sendTelegramNotification,
  deleteFromReceiving,
  updateSettingsByIp,
  addToReceivingList,
  deleteFromSending,
  removeFromErrors,
  addToSendingList,
  getSettingsByIp,
  notifySuccess,
  getSyncFiles,
  clearDirSync,
  getSettings,
  backUpFiles,
  notifyError,
  backUpData,
  isDirEmpty,
  unlockSync,
  ioMessage,
  saveData,
  lockSync,
  getData,
  getIp,
  post,
};