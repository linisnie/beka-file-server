let path = require('path');
let fs = require('fs-extra');
let functions = require('./functions');
let log = require('./system/log')(module);
let dayjs = require('dayjs');
const {DIR_IN_NOT_EMPTY, DIR_IN_LOCKED, DIR_NOT_EXIST, SIGNAL_DOES_NOT_EXISTS} = require('./constants');
let constants = require('./constants');
const {unlockSync} = require("./functions");

/**
 * Upload file to input directory from remote computer
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function uploadFile(req, res) {
  let ip = functions.getIp(req);
  let computer = functions.getSettingsByIp(ip);
  req.pipe(req.busboy);
  log.info(`start uploading from ${ip}`);
  req.busboy.on('file', (fieldName, file, filename) => {
    log.info(`uploading ${filename} from ${ip}`);
    fs.ensureDirSync(path.join(computer.inputDir));
    const fstream = fs.createWriteStream(path.join(computer.inputDir, filename), {mode: 0o777});
    file.pipe(fstream);
    let bytes = 0;
    let size = req.headers['content-length'];
    let percent = 0, lastPercent = 0;
    file.on('data', (chunk) => {
      let sent = bytes += chunk.length;
      percent = Math.round(sent/size*100);
      if (percent > lastPercent + 5 || (percent === 100 && lastPercent !== 100)) {
        lastPercent = percent;
        functions.ioMessage(
          {host: ip, path: ['ioData', 'computers', ip, 'uploading', filename],
            value: {name: filename, loaded: percent}}
        );
      }
    });
    let timeout = () => {
      file.unpipe();
      fstream.destroy();
      log.info(`delete file ${path.join(computer.inputDir, filename)} because of connection lost`);
      try {
        fs.unlinkSync(path.join(computer.inputDir, filename));
      } catch (e) {
        log.error(e);
      }
      res.end(JSON.stringify({status: 'error', message: 'not uploaded because of timeout', filename}));
    }
    req.on("timeout", timeout);
    setTimeout(() => {
      log.error(`close uploading file because of timeout for 1 hour`);
      timeout();
    }, 1000 * 60 * 60);

    fstream.on('finish', () => {
      res.set({
        'Content-Type': 'application/json',
      });
      log.info(`file ${filename} uploaded from ${ip}`);
      res.end(JSON.stringify({status: 'ok', message: 'uploaded', filename}));
    });
  });
}

/**
 * Getting upload request
 * checking local directory for lock/empty
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getRequestUploading(req, res) {
  let computer = getComputer(req);
  let host = computer.host;
  let uploadPath = computer.inputDir;
  log.info(`upload request received from ${host}`);
  if (constants.process.shutDownSignalReceived || !constants.process.isRunning) {
    endResponse(res, host, constants.status.STOPPED, 'error');
    log.info(`upload request from ${host} ended because server stopped`);
    return;
  }
  if (!computer.enabled) {
    endResponse(res, host, constants.status.SYNC_DISABLED, 'error');
    return;
  }
  let fd = canUploadToDir(uploadPath, computer.syncFiles);

  if ([DIR_IN_LOCKED, DIR_IN_NOT_EMPTY, DIR_NOT_EXIST, SIGNAL_DOES_NOT_EXISTS].includes(fd)) {
    endResponse(res, host, fd, 'error');
    log.info(`upload request from ${host} ended because ${fd}`);
    if (fd !== DIR_IN_NOT_EMPTY)
      functions.notifyError('upload', computer.lastSent, host, computer.name, fd);
  }
  else {
    addUnlockTimer(uploadPath, fd, true, host);
    functions.addToReceivingList(host);
    log.info(`upload request from ${host} can start uploading`);
    endResponse(res, host, constants.status.UPLOADING);
  }
}

function endResponse(res, host, message, status = 'ok') {
  res.end(JSON.stringify({status, message}));
  functions.updateSettingsByIp(host, {uploadingStatus: message});
  functions.ioMessage({host, path: ['ioData', 'computers', host, 'uploadingStatus'], value: message});
}

/**
 * Extending local directory unlock timer for longer time
 * Needed for big files
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getRequestExtendUnlocking(req, res) {
  let {inputDir, host} = getComputer(req);
  log.info(`extend unlock timer request received for ${host}`);
  extendUnlockTimer(inputDir, host);
  res.end(JSON.stringify({status: 'ok', message: 'extended'}));
}

/**
 * Request for ending upload operation
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getRequestEndUploading(req, res) {
  let {inputDir, host} = getComputer(req);
  let {syncFiles} = functions.getSettingsByIp(host);
  let data = functions.getData();
  log.info(`end uploading signal received from ${host}`);
  functions.backUpFiles(inputDir, syncFiles);
  if (data.settings.debugMode)
    functions.clearDirSync(inputDir);
  functions.unlockSync(getFd(inputDir), inputDir);
  clearUnlockTimer(inputDir);
  functions.updateSettingsByIp(host, {lastUpload: dayjs().format(), uploadingStatus: constants.status.UPLOADED});
  res.end(JSON.stringify({status: 'ok', message: 'ended'}));
  functions.deleteFromReceiving(host);
  functions.ioMessage({host, path: ['ioData', 'computers', host, 'uploadingStatus'], value: constants.status.UPLOADED});
  functions.ioMessage({host, path: ['ioData', 'computers', host, 'lastUpload'], value: dayjs().format()});
  functions.ioMessage({host, path: ['ioData', 'computers', host, 'uploading'], value: {}});
  functions.removeFromErrors('upload', host);
}

async function getRequestRevoke(req, res) {
  let {inputDir, host} = getComputer(req);
  log.info(`revoke signal received from ${host}`);
  let fd = getFd(inputDir);
  clearUnlockTimer(inputDir);
  try {
    functions.clearDirSync(inputDir);
  } catch(e) {}
  if (fd)
    functions.unlockSync(fd, inputDir);
  functions.deleteFromReceiving(host);
  res.end(JSON.stringify({status: 'ok', message: 'revoked'}));
}

/**
 * Find computer by its ip address (from request)
 * @param req
 * @returns {boolean|object}
 */
function getComputer(req) {
  let ip = functions.getIp(req);
  let computer = functions.getSettingsByIp(ip);
  if (!computer)
    log.error(`computer not found, ${ip}`);
  return computer;
}

let inputDirectories = {};

/**
 * Adding timer to unlock and clear directory
 * @param {string} dirPath - directory path
 * @param {number} fd - opened file descriptor
 * @param {boolean} shouldLog - add this operation to log file
 * @param {string} host - ip of host
 */
function addUnlockTimer(dirPath, fd, shouldLog = true, host) {
  if (shouldLog)
    log.info(`adding unlock timer for ${dirPath}`);
  clearUnlockTimer(dirPath, false);
  let timer = setTimeout(() => {
    // fs.close(fd, err => {
    //   if (err) log.error(err);
    // });
    unlockSync(fd, dirPath);
    functions.clearDirSync(dirPath);
    functions.deleteFromReceiving(host);
    log.info(`unlock and clear ${dirPath} by timer`);
  },constants.UNLOCK_TIMER_PERIOD);
  inputDirectories[dirPath] = {
    fd, timer
  }
}

function extendUnlockTimer(dirPath, host) {
  log.info(`extending unlock timer for ${dirPath}`);
  let fd = getFd(dirPath);
  clearUnlockTimer(dirPath, false);
  addUnlockTimer(dirPath, fd, false, host);
}

/**
 *
 * @param dirPath {string} - directory path
 * @param shouldLog {boolean} - should be logged
 */
function clearUnlockTimer(dirPath, shouldLog = true) {
  if (shouldLog)
    log.info(`clearing unlock timer for ${dirPath}`);
  if (inputDirectories[dirPath] && inputDirectories[dirPath].timer) {
    clearTimeout(inputDirectories[dirPath].timer);
    inputDirectories[dirPath].timer = void 0;
    inputDirectories[dirPath].fd = void 0;
  }
}

/**
 * Get file descriptor from inputDirectories
 * @param dirPath {string} - directory path
 * @returns {boolean|number}
 */
function getFd(dirPath) {
  if (inputDirectories[dirPath] && inputDirectories[dirPath].fd)
    return inputDirectories[dirPath].fd;
  log.error(`can't find fd for ${dirPath}`);
  return false;
}

/**
 * Check directory for file upload
 * @param dirPath {string} - directory path
 * @param syncFiles {array} - list of files to sync
 * @returns {string|number} - file descriptor or error message
 */
function canUploadToDir(dirPath, syncFiles) {
  const settings = functions.getSettings();
  if (settings.mode === '1C')
    if (fs.existsSync(path.join(dirPath, constants.LOCKED_BY_1C)))
      return DIR_IN_LOCKED;
  if (settings.mode === 'beka') {
    if (!fs.existsSync(path.join(dirPath, constants.LOCK_FILE)))
      return constants.SIGNAL_DOES_NOT_EXISTS;
    if (getFd(dirPath)) return DIR_IN_LOCKED;
  }

  let isDirEmpty;
  try {
    isDirEmpty = functions.isDirEmpty(dirPath, syncFiles);
  } catch(e) {
    return DIR_NOT_EXIST;
  }
  if (!isDirEmpty)
    return DIR_IN_NOT_EMPTY;

  let fd = functions.lockSync(dirPath);

  if (!fd)
    return DIR_IN_LOCKED;
  return fd;
}

module.exports = {
  uploadFile,
  getRequestUploading,
  getRequestExtendUnlocking,
  getRequestEndUploading,
  getRequestRevoke
};