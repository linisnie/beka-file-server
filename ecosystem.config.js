module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'beka_file_server',
      script    : './server.js',
      output: './logs/pm2/out.log',
      error: './logs/pm2/error.log',
      watch: false,
      kill_timeout: 1000*60*3,
      ignore_watch: ['./node_modules', './logs', './.*', './.vscode', './public'],
      env: {
        NODE_ENV: "development",
      },
      env_production : {
        NODE_ENV: 'production'
      }
    },

  ]
};
